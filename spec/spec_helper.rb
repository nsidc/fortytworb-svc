# frozen_string_literal: true

require 'rack/test'
require 'rspec'

require_relative '../fortytwo_svc.rb'

ENV['RACK_ENV'] = 'test'

module RSpecMixin
  include Rack::Test::Methods
  def app
    described_class
  end
end

RSpec.configure { |c| c.include RSpecMixin }
