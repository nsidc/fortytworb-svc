# frozen_string_literal: true

require_relative './spec_helper.rb'

require_relative '../fortytwo_svc.rb'

describe FortyTwoApp do
  it 'should return 42 at /' do
    get '/'

    expect(last_response).to be_ok
    expect(last_response.body).to eq('42')
  end

  it 'should give help' do
    get '/help'

    expect(last_response).to be_ok
    expect(last_response.body).to eq("Don't Panic!")
  end
end
