# frozen_string_literal: true

require 'fortytwo'
require 'sinatra'
require 'sinatra/base'

# simple app to return the Answer to the Ultimate Question of Life, the
# Universe, and Everything
class FortyTwoApp < Sinatra::Base
  set :bind, '0.0.0.0'

  get '/' do
    FortyTwo.fortytwo.to_s
  end

  get '/help' do
    "Don't Panic!"
  end
end
