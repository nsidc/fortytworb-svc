# v1.0.0 (4/20/2018)

* Switch to the MIT license.
* Simplify the README so it's the same as other `fortytwo*-svc`
  projects.
