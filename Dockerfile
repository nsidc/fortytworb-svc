FROM ruby:2.3.4-alpine

RUN apk add --update net-tools

ENV APP_HOME /app

RUN mkdir $APP_HOME
ADD . $APP_HOME
WORKDIR $APP_HOME

RUN bundle install

EXPOSE 4567

CMD bundle exec ruby ./fortytwo_svc.rb
