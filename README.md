FortyTwo RB Service
---

A Ruby Sinatra service to demonstrate how to test, build, and publish
a Docker image to [Docker Hub](https://hub.docker.com/r/nsidc/fortytworb-svc)
using [CircleCI](https://circleci.com/nsidc/fortytworb-svc).

[![CircleCI](https://circleci.com/bb/nsidc/fortytworb-svc.svg?style=svg)](https://circleci.com/bb/nsidc/fortytworb-svc)

* [DockerHub: fortytwojs-svc](https://hub.docker.com/r/nsidc/fortytwojs-svc/)

Prerequisites
---

* Ruby 2.3
* Bundler

Development
---

Install dependencies:

    $ bundle install

Run the service locally:

    $ bundle exec ruby ./fortytwo_svc.rb

Build and run via Docker:

    $ docker build -t fortytworb-svc:dev .
    $ docker run -p 4567:4567 --name 42 --rm fortytworb-svc:dev

Workflow
---

TL;DR: Use
[GitHub Flow](https://guides.github.com/introduction/flow/index.html).

In more detail:

1. Create a feature branch.
2. Create and push commits on that branch.
3. The feature branch will get built on CircleCI with each push.
4. Update the CHANGELOG with description of changes.
5. Create a Pull Request on BitBucket.
6. When the feature PR is merged, master will get built on CircleCI.

Releasing
---

1. Update the CHANGELOG to list the new version.
2. Add files and commit

        $ git add CHANGELOG.md ...
        $ git commit -m "Release v.X.Y.Z"

3. Create a new version tag:

        $ git tag v1.2.3

4. Push

        $ git push origin master --tags

The new version will get built in CircleCI and pushed
to [Docker Hub](https://hub.docker.com/) with the corresponding
version as a Docker image tag.

Installing and Running
---

To install and run the service:

    $ docker pull nsidc/fortytworb-svc
    $ docker run -p 5000:5000 --name fortytworb-svc nsidc/fortytworb-svc

License
---

Copyright 2018 National Snow and Ice Datacenter (NSIDC)
<programmers@nsidc.org>

This code is licensed and distributed under the terms of the MIT
License (see LICENSE).
